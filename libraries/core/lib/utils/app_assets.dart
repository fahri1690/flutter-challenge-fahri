class AppAssets {
  static const String _baseImage = 'assets/images';
  static const String splashHeaderImage = '$_baseImage/header-splash.png';
  static const String logo = '$_baseImage/logo.png';
  static const String splashFooterImage = '$_baseImage/footer-splash.png';
  static const String loginHeaderImage = '$_baseImage/header-login.png';
}