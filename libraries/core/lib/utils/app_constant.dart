class AppConstant {
  static const int ok = 200;
  static const int badRequest = 400;
  static const int notFound = 404;
}