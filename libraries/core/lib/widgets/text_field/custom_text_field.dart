import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final ValueChanged<String>? onChanged;
  final String? hintText;
  final String? initialValue;
  final bool obscureText;
  final Widget? suffixIcon;

  const CustomTextField({
    Key? key,
    this.onChanged,
    this.hintText,
    this.initialValue,
    this.suffixIcon,
    this.obscureText = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        hintText: hintText,
        suffixIcon: suffixIcon,
      ),
      onChanged: (val) => onChanged!(val),
      obscureText: obscureText,
    );
  }
}
