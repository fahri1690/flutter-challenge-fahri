import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final VoidCallback onPressed;
  final Widget child;
  final bool showProgressIndicator;

  const CustomButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.showProgressIndicator = false,
  }) : super(key: key);

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    debugPrint('${widget.showProgressIndicator}');
    return SizedBox(
      height: 44,
      child: ElevatedButton(
        onPressed: widget.showProgressIndicator ? null : widget.onPressed,
        child: Center(
          child: widget.showProgressIndicator
              ? const SizedBox(
                  height: 30,
                  width: 30,
                  child: CircularProgressIndicator(
                    strokeWidth: 3,
                  ),
                )
              : widget.child,
        ),
      ),
    );
  }
}
