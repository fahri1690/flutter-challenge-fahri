import 'package:core/core.dart';
import 'package:dependencies/dependencies.dart';

import 'presentation/ui/authentication_page.dart';
import 'presentation/ui/home_page.dart';

class SplashModule extends Module{

  @override
  List<ModularRoute> get routes => [
    ChildRoute(
      Modular.initialRoute,
      child: (_, args) => AuthenticationPage()
    ),
    ChildRoute(
      AppRoutes.homePage,
      child: (_, args) => HomePage(title: '${args.data}'),
    ),
  ];
}