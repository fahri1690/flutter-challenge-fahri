import 'package:core/core.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {

  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(AppAssets.splashHeaderImage),
          Expanded(child: Image.asset(AppAssets.logo)),
          Image.asset(AppAssets.splashFooterImage),
        ],
      ),
    );
  }
}
