import 'package:core/core.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splash/presentation/bloc/splash_cubit.dart';
import 'package:splash/presentation/ui/splash_screen.dart';

import 'login_page.dart';

class AuthenticationPage extends StatelessWidget {
  AuthenticationPage({Key? key}) : super(key: key);

  final _cubit = AuthenticationCubit();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => _cubit..startAuthentication(),
        child: BlocConsumer<AuthenticationCubit, AuthenticationState>(
          listener: (context, state) {
            if (!state.isInitial) {
              SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
                  overlays: SystemUiOverlay.values);
            }
            if (state.hasError) {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: const Text('Gagal login'),
                    content: Text(state.message),
                    actions: [
                      TextButton(
                        onPressed: () => Modular.to.pop(),
                        child: const Text('OK'),
                      )
                    ],
                  );
                },
              );
            }
            if (state.isLoading) {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Row(
                      children: const [
                        SizedBox(
                          height: 30,
                          width: 30,
                          child: Center(child: CircularProgressIndicator()),
                        ),
                        Padding(
                          padding: EdgeInsets.all(12.0),
                          child: Text('Authenticating...'),
                        )
                      ],
                    ),
                  );
                },
              );
            }
            if (state.isSuccess) {
              Modular.to.pushReplacementNamed(AppRoutes.homePage,
                  arguments: state.userName);
            }
          },
          builder: (context, state) {
            if (state.isInitial) {
              return const SplashScreen();
            } else {
              return LoginPage(
                state: state,
                cubit: _cubit,
              );
            }
          },
        ),
      ),
    );
  }
}
