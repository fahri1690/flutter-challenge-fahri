import 'package:core/core.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:splash/presentation/bloc/splash_cubit.dart';

class LoginPage extends StatelessWidget {
  final AuthenticationState state;
  final AuthenticationCubit cubit;

  const LoginPage({Key? key, required this.state, required this.cubit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height,
          ),
          child: IntrinsicHeight(
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset(AppAssets.loginHeaderImage,
                        width: MediaQuery.of(context).size.width / 3),
                    Image.asset(AppAssets.logo,
                        width: MediaQuery.of(context).size.width / 3),
                  ],
                ),
                Expanded(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Login',
                            style:
                                TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 12.0),
                            child: Text(
                              'Please sign in to continue',
                            ),
                          ),
                          CustomTextField(
                            initialValue: state.userName,
                            hintText: 'User ID',
                            onChanged: (val) {
                              cubit.changeUserName(val);
                            },
                          ),
                          CustomTextField(
                            obscureText: state.isObscure,
                            initialValue: state.password,
                            hintText: 'Password',
                            onChanged: (val) {
                              cubit.changePassword(val);
                            },
                            suffixIcon: IconButton(
                              icon: state.isObscure
                                  ? const Icon(Icons.remove_red_eye_outlined)
                                  : const Icon(Icons.remove_red_eye),
                              onPressed: () {
                                cubit.obscureText(state.isObscure);
                              },
                            ),
                          ),
                          CustomButton(
                            onPressed: () {
                              FocusNode().dispose();
                              cubit.authenticateUser(state.userName, state.password);
                            },
                            child: const Text('Login'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 50.0),
                  child: RichText(
                    text: TextSpan(
                        style: const TextStyle(color: Colors.grey),
                        children: [
                          const TextSpan(text: 'Don\'t have an account? '),
                          TextSpan(
                              text: 'Sign Up',
                              style: const TextStyle(
                                  fontWeight: FontWeight.w600, color: Colors.red),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () => debugPrint('Tapped Sign up')),
                        ]),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
