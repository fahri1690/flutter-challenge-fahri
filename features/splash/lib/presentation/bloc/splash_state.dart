part of 'splash_cubit.dart';

class AuthenticationState {
  final bool isInitial;
  final bool isLoading;
  final bool hasError;
  final bool isSuccess;
  final String userName;
  final String password;
  final String message;
  final bool isObscure;

  AuthenticationState({
    this.isInitial = false,
    this.isLoading = false,
    this.hasError = false,
    this.isSuccess = false,
    this.isObscure = true,
    this.userName = '',
    this.password = '',
    this.message = '',
  });

  AuthenticationState copyWith({
    bool? isInitial,
    bool? hasError,
    bool? isSuccess,
    bool? isLoading,
    bool? isObscure,
    String? userName,
    String? password,
    String? message,
  }) {
    return AuthenticationState(
      userName: userName ?? this.userName,
      password: password ?? this.password,
      message: message ?? this.message,
      hasError: hasError ?? this.hasError,
      isSuccess: isSuccess ?? this.isSuccess,
      isInitial: isInitial ?? this.isInitial,
      isLoading: isLoading ?? this.isLoading,
      isObscure: isObscure ?? this.isObscure,
    );
  }
}
