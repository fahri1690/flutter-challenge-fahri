import 'package:core/core.dart';
import 'package:dependencies/dependencies.dart';
import 'package:splash/domain/use_case/authentication_use_case.dart';

part 'splash_state.dart';

class AuthenticationCubit extends Cubit<AuthenticationState> {
  AuthenticationCubit() : super(AuthenticationState());

  void startAuthentication() async {
    emit(state.copyWith(isInitial: true));
    await Future.delayed(const Duration(seconds: 2));
    emit(state.copyWith(isInitial: false));
  }

  void authenticateUser(String userName, String password) async {
    emit(state.copyWith(isLoading: true, isSuccess: false, hasError: false));
    final result = await Modular.get<AuthenticationUseCase>()
        .authenticateUser(userName, password);
    if (result == AppConstant.badRequest) {
      emit(state.copyWith(
          isLoading: false,
          isSuccess: false,
          hasError: true,
          message: 'User ID atau password kosong'));
    } else if (result == AppConstant.notFound) {
      emit(state.copyWith(
          isLoading: false,
          isSuccess: false,
          hasError: true,
          message: 'User ID atau password salah'));
    } else {
      emit(state.copyWith(
          isLoading: false,
          hasError: false,
          isSuccess: true,
          message: 'Login Sukses'));
    }
    Modular.to.pop();
  }

  void changeUserName(String val) {
    emit(state.copyWith(hasError: false, userName: val));
  }

  void changePassword(String val) {
    emit(state.copyWith(hasError: false, password: val));
  }

  void obscureText(bool isObscure) {
    emit(state.copyWith(hasError: false, isObscure: !isObscure));
  }
}
