import 'package:core/core.dart';
import 'package:splash/domain/entities/user.dart';

abstract class AuthenticationRemoteDataSource {
  Future<int> authenticateUser(String userId, String password);
}

class AuthenticationRemoteDataSourceImpl
    implements AuthenticationRemoteDataSource {
  @override
  Future<int> authenticateUser(String userId, String password) async {
    final user = User();
    await Future.delayed(const Duration(seconds: 3));
    if (userId.isEmpty || password.isEmpty) {
      return AppConstant.badRequest;
    } else if (user.name != userId || password != user.password) {
      return AppConstant.notFound;
    } else {
      return AppConstant.ok;
    }
  }
}
