import 'package:dependencies/dependencies.dart';
import 'package:splash/data/sources/remote/authentication_remote_data_source.dart';

abstract class AuthenticationRemoteRepository {
  Future<int> authenticateUser(String userId, String password);
}

class AuthenticationRemoteRepositoryImpl
    implements AuthenticationRemoteRepository {
  @override
  Future<int> authenticateUser(String userId, String password) =>
      Modular.get<AuthenticationRemoteDataSource>()
          .authenticateUser(userId, password);
}
