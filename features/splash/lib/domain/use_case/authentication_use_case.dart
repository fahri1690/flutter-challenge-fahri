import 'package:dependencies/dependencies.dart';

import '../repositories/authentication_remote_repositories.dart';

abstract class AuthenticationUseCase {
  Future<int> authenticateUser(String userId, String password);
}

class AuthenticationUseCaseImpl implements AuthenticationUseCase {
  @override
  Future<int> authenticateUser(String userId, String password) =>
      Modular.get<AuthenticationRepository>()
          .authenticateUser(userId, password);
}
