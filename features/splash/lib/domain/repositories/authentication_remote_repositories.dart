import 'package:dependencies/dependencies.dart';
import 'package:splash/data/repositories/authentication_remote_repositories.dart';

abstract class AuthenticationRepository {
  Future<int> authenticateUser(String userId, String password);
}

class AuthenticationRepositoryImpl
    implements AuthenticationRepository {
  @override
  Future<int> authenticateUser(String userId, String password) =>
      Modular.get<AuthenticationRemoteRepository>()
          .authenticateUser(userId, password);
}
