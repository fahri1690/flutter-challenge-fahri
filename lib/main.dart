import 'package:core/core.dart';
import 'package:dependencies/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:splash/data/repositories/authentication_remote_repositories.dart';
import 'package:splash/data/sources/remote/authentication_remote_data_source.dart';
import 'package:splash/domain/repositories/authentication_remote_repositories.dart';
import 'package:splash/domain/use_case/authentication_use_case.dart';
import 'package:splash/splash_module.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack);
  runApp(
    ModularApp(
      module: AppModule(),
      child: const MyApp(),
    ),
  );
}

class AppModule extends Module {
  @override
  List<Bind<Object>> get binds => [
        Bind.singleton((i) => AppRoutes()),
        Bind.singleton((i) => AuthenticationRemoteDataSourceImpl()),
        Bind.singleton((i) => AuthenticationRemoteRepositoryImpl()),
        Bind.singleton((i) => AuthenticationRepositoryImpl()),
        Bind.singleton((i) => AuthenticationUseCaseImpl()),
      ];

  @override
  List<ModularRoute> get routes => [
        ModuleRoute(Modular.initialRoute, module: SplashModule()),
      ];
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate: Modular.routerDelegate,
      routeInformationParser: Modular.routeInformationParser,
      builder: (context, widget) {
        return widget!;
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: AppColors.primary,
      ),
    );
  }
}
